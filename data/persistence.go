package data

import (
	"time"
)

type DBFields struct {
	RId       uint32    `json:"-" xml:"-"`
	Created   time.Time `json:"-" xml:"-"`
	CreatedBy *User     `json:"-" xml:"-"`
	Updated   time.Time `json:"-" xml:"-"`
	UpdatedBy *User     `json:"-" xml:"-"`
}

func (f *DBFields) GetCreated() time.Time {
	return f.Created
}

func (f *DBFields) GetUpdated() time.Time {
	return f.Updated
}

func (f *DBFields) SetTimestamps(newResource bool) {
	timestamp := time.Now().UTC()
	if newResource {
		f.Created = timestamp
	}
	f.Updated = timestamp
}
