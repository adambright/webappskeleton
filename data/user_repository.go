package data

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/go-say/say"
	_ "github.com/go-sql-driver/mysql"

	"github.com/alittlebrighter/webAppSkeleton/util"
)

const userSqlStrings string = "r_id, id, full_name, first_name, email_address, password, token, created, created_by, updated, updated_by"

func (u *User) GetParamStrings() (params map[interface{}]string) {
	paramStrings := strings.Split(userSqlStrings, ", ")

	params = make(map[interface{}]string)
	params[&u.Id] = paramStrings[1]
	params[&u.RId] = paramStrings[0]
	params[&u.FullName] = paramStrings[2]
	params[&u.FirstName] = paramStrings[3]
	params[&u.EmailAddress] = paramStrings[4]
	params[&u.Password] = paramStrings[5]
	params[&u.Token] = paramStrings[6]
	params[&u.Created] = paramStrings[7]
	params[&u.CreatedBy.RId] = paramStrings[8]
	params[&u.Updated] = paramStrings[9]
	params[&u.UpdatedBy.RId] = paramStrings[10]
	return
}

func (u *User) getSqlParams() (*uint32, *string, *string, *string, *string, *string, *string, *time.Time, *uint32, *time.Time, *uint32) {
	return &u.RId,
		&u.Id,
		&u.FullName,
		&u.FirstName,
		&u.EmailAddress,
		&u.Password,
		&u.Token,
		&u.Created,
		&u.CreatedBy.RId,
		&u.Updated,
		&u.UpdatedBy.RId
}

type UserRepository struct {
	tableName  string
	lazyLoad   bool
	statements map[string]*sql.Stmt
}

func (r *UserRepository) Initialize(table string) {
	say.Info("Initializing UserRepository")

	r.tableName = table

	var err error
	r.statements = make(map[string]*sql.Stmt)
	r.statements["create"], err = db.Prepare(fmt.Sprintf("INSERT INTO %s (%s) VALUES(%s)", r.tableName, userSqlStrings[6:], "?,?,?,?,?,?,?,?,?,?"))
	if err != nil {
		say.Error("cannot prepare create sql", err)
	}
	r.statements["read"], err = db.Prepare(fmt.Sprintf("SELECT %s FROM %s WHERE id = ?", userSqlStrings, r.tableName))
	if err != nil {
		say.Error("cannot prepare read sql", err)
	}
	r.statements["readByRId"], err = db.Prepare(fmt.Sprintf("SELECT %s FROM %s WHERE r_id = ?", userSqlStrings, r.tableName))
	if err != nil {
		say.Error("cannot prepare read sql", err)
	}
	r.statements["readByEmail"], err = db.Prepare(fmt.Sprintf("SELECT %s FROM %s WHERE email_address = ?", userSqlStrings, r.tableName))
	if err != nil {
		say.Error("cannot prepare read sql", err)
	}
	r.statements["setToken"], err = db.Prepare(fmt.Sprintf("UPDATE %s SET token = ? WHERE email_address = ?", r.tableName))
	if err != nil {
		say.Error("cannot prepare setToken sql", err)
	}
	return
}

func (r *UserRepository) GetModelPointer() *User {
	u := new(User)
	u.DBFields = new(DBFields)
	u.CreatedBy = new(User)
	u.UpdatedBy = new(User)
	return u
}

func (r *UserRepository) GetModelInstance() User {
	return User{}
}

func (r *UserRepository) Create(data []byte, unmarshaller util.Unmarshaller) (Model, error) {
	u := r.GetModelPointer()
	err := unmarshaller(data, u)
	if err != nil {
		return nil, err
	}

	setModelId(u)
	u.SetTimestamps(true)

	result, err := r.statements["create"].Exec(u.Id, u.FullName, u.FirstName, u.EmailAddress, u.Password, u.Token, u.Created.String(), u.CreatedBy.RId, u.Updated.String(), u.UpdatedBy.RId)
	if err == nil {
		newId, err := result.LastInsertId()
		if err != nil {
			say.Error("cannot retrieve new record id", err)
		}
		u.RId = uint32(newId)
	}
	return u, err
}

func (r *UserRepository) Read(id string) (Model, error) {
	u := r.GetModelPointer()
	err := r.statements["read"].QueryRow(id).Scan(u.getSqlParams())
	return u, err
}

func (r *UserRepository) ReadByRID(rid uint32) (Model, error) {
	u := r.GetModelPointer()
	err := r.statements["readByRId"].QueryRow(rid).Scan(u.getSqlParams())
	return u, err
}

func (r *UserRepository) ReadByEmail(email string) (*User, error) {
	u := r.GetModelPointer()
	err := r.statements["readByEmail"].QueryRow(email).Scan(u.getSqlParams())
	return u, err
}

func (r *UserRepository) FilterExact(u User) ([]Model, error) {
	return nil, nil
}

func (r *UserRepository) SetToken(u *User) error {
	_, err := r.statements["setToken"].Exec(u.Token, u.EmailAddress)
	return err
}
