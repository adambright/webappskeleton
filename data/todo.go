package data

import (
	"time"
)

type Todo struct {
	Id                string    `json:"id" xml:"id,attr"`
	Description       string    `json:"description" xml:"description"`
	Complete          bool      `json:"complete" xml:"complete"`
	CompleteTimestamp time.Time `json:"completeTimestamp" xml:"complete-timestamp"`
	*DBFields
}

func (m *Todo) GetId() string {
	return m.Id
}

func (m *Todo) SetId(id string) Model {
	m.Id = id
	return m
}
