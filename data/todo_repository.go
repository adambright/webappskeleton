package data

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/go-say/say"
	_ "github.com/go-sql-driver/mysql"

	"github.com/alittlebrighter/webAppSkeleton/util"
)

const todoSqlStrings string = "r_id, created, created_by, updated, updated_by, id, description, complete, complete_timestamp"

func (m *Todo) GetParamStrings() (params map[interface{}]string) {
	paramStrings := strings.Split(todoSqlStrings, ", ")

	params = make(map[interface{}]string)
	params[&m.RId] = paramStrings[0]
	params[&m.Created] = paramStrings[1]
	params[&m.CreatedBy] = paramStrings[2]
	params[&m.Updated] = paramStrings[3]
	params[&m.UpdatedBy] = paramStrings[4]
	params[&m.Id] = paramStrings[5]
	params[&m.Description] = paramStrings[6]
	params[&m.Complete] = paramStrings[7]
	params[&m.CompleteTimestamp] = paramStrings[8]
	return
}

func (m *Todo) getSqlParams() (*uint32, *time.Time, *uint32, *time.Time, *uint32, *string, *string, *bool, *time.Time) {
	return &m.RId,
		&m.Created,
		&m.CreatedBy.RId,
		&m.Updated,
		&m.UpdatedBy.RId,
		&m.Id,
		&m.Description,
		&m.Complete,
		&m.CompleteTimestamp
}

type TodoRepository struct {
	tableName  string
	statements map[string]*sql.Stmt
}

func (r *TodoRepository) Initialize(table string) {
	say.Info("Initializing TodoRepository")

	r.tableName = table

	var err error
	r.statements = make(map[string]*sql.Stmt)
	r.statements["create"], err = db.Prepare(fmt.Sprintf("INSERT INTO %s (%s) VALUES(?, ?, ?, ?, ?, ?, ?, ?)", r.tableName, todoSqlStrings[6:]))
	if err != nil {
		say.Error("cannot prepare create sql", err)
	}
	r.statements["read"], err = db.Prepare(fmt.Sprintf("SELECT %s FROM %s WHERE id = ?", todoSqlStrings, r.tableName))
	if err != nil {
		say.Error("cannot prepare read sql", err)
	}
	return
}

func (r *TodoRepository) GetModelPointer() *Todo {
	m := new(Todo)
	m.DBFields = new(DBFields)
	m.CreatedBy = new(User)
	m.UpdatedBy = new(User)
	return m
}

func (r *TodoRepository) GetModelInstance() Todo {
	return Todo{}
}

func (r *TodoRepository) Create(data []byte, unmarshaller util.Unmarshaller) (Model, error) {
	m := r.GetModelPointer()
	err := unmarshaller(data, m)
	if err != nil {
		return nil, err
	}

	setModelId(m)
	m.SetTimestamps(true)

	result, err := r.statements["create"].Exec(m.Created.String(),
		m.CreatedBy.RId,
		m.Updated.String(),
		m.UpdatedBy.RId,
		m.Id,
		m.Description,
		m.Complete,
		m.CompleteTimestamp.String())
	if err == nil {
		newId, err := result.LastInsertId()
		if err != nil {
			say.Error("cannot retrieve new record id", err)
		}
		m.RId = uint32(newId)
	}
	return m, err
}

func (r *TodoRepository) Read(id string) (Model, error) {
	m := r.GetModelPointer()
	err := r.statements["read"].QueryRow(id).Scan(m.getSqlParams())
	if err != nil {
		return nil, err
	}
	return m, err
}
