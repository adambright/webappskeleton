package data

import ()

type User struct {
	Id           string `json:"id" xml:"id,attr"`
	FullName     string `json:"fullName" xml:"full-name"`
	FirstName    string `json:"firstName" xml:"first-name"`
	EmailAddress string `json:"emailAddress" xml:"email-address"`
	Password     string `json:"-" xml:"-"`
	Token        string `json:"token" xml:"token"`
	*DBFields
}

func (u *User) GetId() string {
	return u.Id
}

func (u *User) SetId(id string) Model {
	u.Id = id
	return u
}
