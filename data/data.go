package data

import (
	"database/sql"

	"github.com/go-say/say"
	"github.com/satori/go.uuid"

	"github.com/alittlebrighter/webAppSkeleton/configuration"
	"github.com/alittlebrighter/webAppSkeleton/util"
)

var (
	db       *sql.DB
	TodoRepo *TodoRepository
	UserRepo *UserRepository
	repoList map[string]Repository
)

func Initialize(config *configuration.Data) (err error) {
	db, err = sql.Open(config.Persistence.Dialect, config.Persistence.ConnectionString)
	if err != nil {
		return
	}
	say.Info("Connected to " + config.Persistence.Dialect + " database.")

	UserRepo = new(UserRepository)
	TodoRepo = new(TodoRepository)

	repoList = make(map[string]Repository)
	repoList["users"] = UserRepo
	repoList["todos"] = TodoRepo

	for name, repo := range repoList {
		repo.Initialize(name)
	}
	return
}

func CleanUp() error {
	return db.Close()
}

type Repository interface {
	Initialize(string)
	Create([]byte, util.Unmarshaller) (Model, error)
	Read(string) (Model, error)
}

func GetRepository(m string) Repository {
	return repoList[m]
}

type Model interface {
	GetId() string
	SetId(string) Model
}

func setModelId(m Model) Model {
	m.SetId(uuid.NewV4().String())
	return m
}
