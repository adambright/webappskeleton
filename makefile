CC=go
export GOPATH=${PWD}

debug: 
	$(CC) run src/github.com/alittlebrighter/webAppSkeleton/main.go config.yml

build: clean
	$(CC) install github.com/alittlebrighter/webAppSkeleton

deps:
	$(CC) get gopkg.in/yaml.v2
	$(CC) get github.com/justinas/alice
	$(CC) get github.com/julienschmidt/httprouter
	$(CC) get github.com/dgrijalva/jwt-go
	$(CC) get github.com/hoisie/mustache
	$(CC) get github.com/go-sql-driver/mysql
	$(CC) get github.com/go-say/say
	$(CC) get github.com/satori/go.uuid

.PHONY: clean
clean:
	rm -rf bin/*