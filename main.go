package main

import (
	//"fmt"
	"os"
	"sync"

	"github.com/go-say/say"

	"github.com/alittlebrighter/webAppSkeleton/configuration"
	"github.com/alittlebrighter/webAppSkeleton/data"
	"github.com/alittlebrighter/webAppSkeleton/io/http"
	"github.com/alittlebrighter/webAppSkeleton/security"
)

func main() {
	defer say.CapturePanic()

	config, err := configuration.ReadConfig(os.Args[1])
	if err != nil {
		say.Fatal("Error reading configuration file.", "error", err)
		return
	}
	//mode := "PRODUCTION"
	if config.Debug {
		say.SetDebug(config.Debug)
		//mode = "DEBUG"
	}
	//say.Init(fmt.Sprintf("Starting %s application in %s mode.", config.General.Name, mode))

	err = data.Initialize(&config.Data)
	if err != nil {
		say.Fatal("Error connecting to data source.", "error", err)
		return
	}
	defer data.CleanUp()

	security.Initialize(&config.Security)

	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		defer wg.Done()

		http.Start(&config)
	}()
	say.Info("Web server started.")
	wg.Wait()
}
