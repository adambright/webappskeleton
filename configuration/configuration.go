package configuration

import (
	"io/ioutil"

	"github.com/alittlebrighter/webAppSkeleton/util"
)

type Config struct {
	Debug   bool
	General struct {
		Name        string
		Version     string
		Description string
	}
	Data Data
	Io   struct {
		Http struct {
			HostName       string
			Port           uint16
			ReadTimeout    int64
			WriteTimeout   int64
			ServeStatic    bool
			StaticFilesDir string
			PageSize       uint8
		}
	}
	Security Security
}

type Data struct {
	Persistence struct {
		Dialect          string
		ConnectionString string
	}
}

type Security struct {
	Authentication struct {
		Providers []string
		Google    struct {
			IssClaimContains string
			ClientJsonPath   string
			ClientJson       GoogleClient
		}
	}
}

type GoogleClient struct {
	Web struct {
		Client_id                   string
		Auth_uri                    string
		Auth_provider_x509_cert_url string
	}
}

func ReadConfig(filepath string) (conf Config, err error) {
	err = parseFile(filepath, &conf)
	err = parseFile(conf.Security.Authentication.Google.ClientJsonPath, &conf.Security.Authentication.Google.ClientJson)
	return
}

func parseFile(filepath string, target interface{}) error {
	fileContents, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}

	err = util.GetUnmarshaller(filepath)(fileContents, target)
	if err != nil {
		return err
	}

	return nil
}
