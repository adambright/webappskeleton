package security

import (
	"crypto/rsa"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-say/say"

	"github.com/alittlebrighter/webAppSkeleton/data"
	"github.com/alittlebrighter/webAppSkeleton/util"
)

var googlePublicKeys []rsa.PublicKey

func RefreshGooglePublicKeys() error {
	resp, err := http.Get(config.Authentication.Google.ClientJson.Web.Auth_provider_x509_cert_url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	keys := make(map[string]string)
	err = json.Unmarshal(body, &keys)
	if err != nil {
		return err
	}

	googlePublicKeys = nil
	for _, val := range keys {
		gPubKey, pErr := jwt.ParseRSAPublicKeyFromPEM([]byte(val))
		if err != nil {
			say.Error(pErr)
			continue
		}
		googlePublicKeys = append(googlePublicKeys, *gPubKey)
	}
	return nil
}

func ParseGoogleToken(newToken string) (u data.User, err error) {
	if len(googlePublicKeys) == 0 {
		if err = RefreshGooglePublicKeys(); err != nil {
			return
		}
	}

	var badKeys []int
	for i, pubKey := range googlePublicKeys {
		token, err := jwt.Parse(newToken, func(token *jwt.Token) (interface{}, error) {
			if !strings.Contains(token.Claims["iss"].(string), config.Authentication.Google.IssClaimContains) {
				return nil, errors.New("ID token contains invalid \"iss\" claim.")
			}

			if token.Claims["aud"].(string) != config.Authentication.Google.ClientJson.Web.Client_id {
				return nil, errors.New("ID token contains invalid \"aud\" claim.")
			}

			// Don't forget to validate the alg is what you expect:
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, errors.New(fmt.Sprintf("Unexpected signing method: %v", token.Header["alg"]))
			}

			return &pubKey, nil
		})
		if err != nil || !token.Valid {
			say.Debug("Bad public key.", "error", err)
			badKeys = append(badKeys, i)
			continue
		}
		say.Debug("Authentication successful for " + token.Claims["email"].(string))

		u.EmailAddress = token.Claims["email"].(string)
		u.FullName = token.Claims["name"].(string)
		u.FirstName = token.Claims["given_name"].(string)
		break
	}

	googlePublicKeys = util.DeleteArrayIndexes(googlePublicKeys, badKeys)

	return
}
