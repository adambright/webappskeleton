package security

import ()

// AuthDetails is an attempt to make authorization function like linux file permissions
// PrimaryRole acts as the owner of the resource
// SecondaryRole acts as the group for the resource
// Permissions are a 3 character string comprised of integers to specify access to the resource
// 4 = read, 2 = write, 1 = delete
// Permissions[0] declares the
type ResourceAuthorization struct {
	// primary role is the owner of the resource
	PrimaryRole string

	// SecondaryRole is considered correlated to the group the resource is available to
	SecondaryRole string

	// string with length of three mimicking linux file permissions
	// so 644
	Permissions string
}
