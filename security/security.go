package security

import (
	"github.com/alittlebrighter/webAppSkeleton/configuration"
)

var config *configuration.Security

func Initialize(sConfig *configuration.Security) (err error) {
	config = sConfig

	err = RefreshGooglePublicKeys()

	return
}
