## A simple web app skeleton written in Go.

What this provides:
*A simple CRUD API setup that just requires writing a data repository
*Authentication via Google OAuth2 JWT tokens
*Simple way to render static pages via Mustache templates

What this does not have:
*Enforced authentication on any route
*Authorization