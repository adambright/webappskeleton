package util

import (
	"crypto/rsa"
	"encoding/json"
	"encoding/xml"
	"strings"

	yaml "gopkg.in/yaml.v2"
)

type Encode func(interface{}) error

type Unmarshaller func([]byte, interface{}) error

func GetUnmarshaller(mimeType string) (unmarshaller Unmarshaller) {
	switch {
	case strings.HasSuffix(mimeType, "json"):
		unmarshaller = json.Unmarshal
	case strings.HasSuffix(mimeType, "xml"):
		unmarshaller = xml.Unmarshal
	case strings.HasSuffix(mimeType, "yml") || strings.HasSuffix(mimeType, "yaml"):
		unmarshaller = yaml.Unmarshal
	default:
		unmarshaller = json.Unmarshal
	}
	return
}

func Pluralize(s string) (ps string) {
	lastChar := s[len(s)-1]
	switch lastChar {
	case 'y':
		ps = s[:len(s)-1] + "ies"
	default:
		ps = s + "s"
	}
	return
}

func DeleteArrayIndexes(a []rsa.PublicKey, indexes []int) []rsa.PublicKey {
	for index, i := range indexes {
		a[i] = a[len(a)-1]
		a = a[:len(a)-1]

		for sIndex, si := range indexes[index:] {
			if si > i {
				indexes[sIndex] = si - 1
			}
		}
	}
	return a
}
