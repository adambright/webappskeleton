package http

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/go-say/say"

	"github.com/alittlebrighter/webAppSkeleton/data"
	"github.com/alittlebrighter/webAppSkeleton/io/http/context"
)

func (c *appContext) apiCreate(w http.ResponseWriter, r *http.Request) {
	ctx := context.Get(r)
	ctx.Repo = data.GetRepository(ctx.UrlVars[URL_VAR_MODEL].Value)
	var err error
	ctx.ModelOut, err = ctx.Repo.Create(ctx.RequestBody, ctx.Unmarshaller)
	if err != nil {
		say.Error(err)
		http.Error(w, http.StatusText(500), 500)
		ctx.ModelOut = err
	}
	context.Set(r, ctx)
}

func (c *appContext) apiReadAll(w http.ResponseWriter, r *http.Request) {
	reqC := context.Get(r)
	fmt.Fprintf(w, "Reading all "+reqC.UrlVars[URL_VAR_MODEL].Value)
}

func (c *appContext) apiRead(w http.ResponseWriter, r *http.Request) {
	ctx := context.Get(r)
	ctx.Repo = data.GetRepository(ctx.UrlVars[URL_VAR_MODEL].Value)
	instance := strings.Split(ctx.UrlVars[URL_VAR_INSTANCE].Value, ".")
	var err error
	ctx.ModelOut, err = ctx.Repo.Read(instance[0])
	if err != nil {
		say.Error("cannot get entity", err)
		ctx.ModelOut = err
	}
	context.Set(r, ctx)
}

func (c *appContext) apiUpdate(w http.ResponseWriter, r *http.Request) {
	reqC := context.Get(r)
	fmt.Fprintf(w, "Updating instance "+reqC.UrlVars[URL_VAR_INSTANCE].Value+" which is a(n) "+reqC.UrlVars[URL_VAR_MODEL].Value)
}

func (c *appContext) apiUpdatePart(w http.ResponseWriter, r *http.Request) {
	reqC := context.Get(r)
	fmt.Fprintf(w, "Updating part of instance "+reqC.UrlVars[URL_VAR_INSTANCE].Value+" which is a(n) "+reqC.UrlVars[URL_VAR_MODEL].Value)
}

func (c *appContext) apiDelete(w http.ResponseWriter, r *http.Request) {
	reqC := context.Get(r)
	fmt.Fprintf(w, "Deleting instance "+reqC.UrlVars[URL_VAR_INSTANCE].Value+" which is a(n) "+reqC.UrlVars[URL_VAR_MODEL].Value)
}
