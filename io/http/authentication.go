package http

import (
	"net/http"

	"github.com/go-say/say"
	"github.com/satori/go.uuid"

	"github.com/alittlebrighter/webAppSkeleton/data"
	"github.com/alittlebrighter/webAppSkeleton/security"
)

func verifyGoogleIDToken(w http.ResponseWriter, r *http.Request) {
	idToken := r.PostFormValue("idToken")
	if idToken == "" {
		http.Error(w, "Google ID token not received (should be \"idToken\" POST form value).", 422)
	}

	u, err := security.ParseGoogleToken(idToken)
	if err != nil {
		say.Error(err)
		http.Error(w, "Error parsing Google token", 500)
	}

	/*
		existingUser, uErr := data.UserRepo.ReadByEmail(u.EmailAddress)
		if uErr != nil {
			say.Error(uErr)
			http.Error(w, "An error occurred trying to find user.", 500)
			return
		}
	*/
	u.Token = uuid.NewV4().String()
	err = data.UserRepo.SetToken(&u)
	outErr := getEncoder(w, r)(u)
	if outErr != nil {
		say.Error(outErr)
		http.Error(w, "An error occurred encoding the user object.", 500)
		return
	}
}
