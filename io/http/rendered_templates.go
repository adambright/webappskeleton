package http

import (
	"net/http"

	"github.com/hoisie/mustache"
)

func (c *appContext) index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(mustache.RenderFile(c.StaticFilesDir+"templates/index.html.mustache", c)))
}
