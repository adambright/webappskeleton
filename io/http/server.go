package http

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	//"time"

	"github.com/julienschmidt/httprouter"
	"github.com/justinas/alice"

	"github.com/alittlebrighter/webAppSkeleton/configuration"
	"github.com/alittlebrighter/webAppSkeleton/data"
	"github.com/alittlebrighter/webAppSkeleton/io/http/context"
)

const (
	URL_VAR_VERSION int = iota - 1
	URL_VAR_MODEL
	URL_VAR_INSTANCE
	URL_VAR_MIMETYPE

	model    string = "model"
	instance string = "id"
	apiV1    string = "v1"
)

type appContext struct {
	StaticFilesDir string
	PageSize       uint8
	GoogleClientId string
	Users          []data.User
}

// Wrapper to allow standard net/http handlers to be used with httprouter.
func wrapHandler(h http.Handler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		c := context.Get(r)
		c.UrlVars = ps
		context.Set(r, c)
		h.ServeHTTP(w, r)
	}
}

func Start(config *configuration.Config) {
	appC := appContext{
		StaticFilesDir: config.Io.Http.StaticFilesDir,
		PageSize:       config.Io.Http.PageSize,
		GoogleClientId: config.Security.Authentication.Google.ClientJson.Web.Client_id,
	}

	baseMiddleware := alice.New(loggerMiddleware, recoverMiddleware, context.CleanUp)
	apiMiddleware := alice.New(loggerMiddleware, recoverMiddleware, context.CleanUp, bodyDecodeEncode)

	router := httprouter.New()
	if config.Io.Http.ServeStatic {
		router.ServeFiles("/static/*filepath", http.Dir(config.Io.Http.StaticFilesDir))
	}

	// homepage
	router.GET("/", wrapHandler(baseMiddleware.ThenFunc(appC.index)))

	// security
	router.POST("/auth/google/login", wrapHandler(baseMiddleware.ThenFunc(verifyGoogleIDToken)))

	// api endpoints
	modelUrl := fmt.Sprintf("/api/%s/:%s", apiV1, model)
	instanceUrl := fmt.Sprintf("/api/%s/:%s/:%s", apiV1, model, instance)
	router.POST(modelUrl, wrapHandler(apiMiddleware.ThenFunc(appC.apiCreate)))
	router.GET(modelUrl, wrapHandler(apiMiddleware.ThenFunc(appC.apiReadAll)))
	router.GET(instanceUrl, wrapHandler(apiMiddleware.ThenFunc(appC.apiRead)))
	router.PUT(instanceUrl, wrapHandler(apiMiddleware.ThenFunc(appC.apiUpdate)))
	router.PATCH(instanceUrl, wrapHandler(apiMiddleware.ThenFunc(appC.apiUpdate)))
	router.DELETE(instanceUrl, wrapHandler(apiMiddleware.ThenFunc(appC.apiDelete)))
	/*
		webServer := http.Server{
			Address:        config.Io.Http.HostName + ":" + strconv.Itoa(int(config.Io.Http.Port)),
			Handler:        router,
			ReadTimeout:    config.Io.Http.ReadTimeout * time.Second,
			WriteTimeout:   config.Io.Http.WriteTimeout * time.Second,
			MaxHeaderBytes: 1048576,
		}
	*/
	log.Fatal(http.ListenAndServe(config.Io.Http.HostName+":"+strconv.Itoa(int(config.Io.Http.Port)), router))
}
