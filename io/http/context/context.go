// main ideas stolen from gorillacontext
package context

import (
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"

	appData "github.com/alittlebrighter/webAppSkeleton/data"
	"github.com/alittlebrighter/webAppSkeleton/util"
)

type requestContext struct {
	UrlVars      httprouter.Params
	Repo         appData.Repository
	RequestBody  []byte
	Unmarshaller util.Unmarshaller
	ModelOut     interface{}
}

var (
	data  = make(map[*http.Request]requestContext)
	datat = make(map[*http.Request]int64)
)

func Set(r *http.Request, ctx requestContext) {
	data[r] = ctx
}

// Get returns a value stored for a given key in a given request.
func Get(r *http.Request) requestContext {
	return data[r]
}

// Clear removes all values stored for a given request.
//
// This is usually called by a handler wrapper to clean up request
// variables at the end of a request lifetime. See ClearHandler().
func Clear(r *http.Request) {
	delete(data, r)
	delete(datat, r)
}

// Purge removes request data stored for longer than maxAge, in seconds.
// It returns the amount of requests removed.
//
// If maxAge <= 0, all request data is removed.
//
// This is only used for sanity check: in case context cleaning was not
// properly set some request data can be kept forever, consuming an increasing
// amount of memory. In case this is detected, Purge() must be called
// periodically until the problem is fixed.
func Purge(maxAge int) int {
	count := 0
	if maxAge <= 0 {
		count = len(data)
		data = make(map[*http.Request]requestContext)
		datat = make(map[*http.Request]int64)
	} else {
		min := time.Now().Unix() - int64(maxAge)
		for r := range data {
			if datat[r] < min {
				Clear(r)
				count++
			}
		}
	}
	return count
}

// ClearHandler wraps an http.Handler and clears request values at the end
// of a request lifetime.
func CleanUp(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer Clear(r)
		h.ServeHTTP(w, r)
	})
}
