package http

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/go-say/say"

	"github.com/alittlebrighter/webAppSkeleton/io/http/context"
	"github.com/alittlebrighter/webAppSkeleton/util"
)

func loggerMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer say.NewTiming().Say("Response Time", "path", r.URL.String(), "verb", r.Method)
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func recoverMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				panic(err)
				http.Error(w, http.StatusText(500), 500)
			}
		}()

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func bodyDecodeEncode(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := context.Get(r)
		defer r.Body.Close()

		// prevent overflow attacks with massive request bodies
		var err error
		ctx.RequestBody, err = ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
		if err != nil {
			say.Error(err)
			w.WriteHeader(422) // unprocessable entity
			ctx.ModelOut = err
		} else {
			ctx.Unmarshaller = util.GetUnmarshaller(getMimeType(r))
			context.Set(r, ctx)
			next.ServeHTTP(w, r)
		}
		ctx = context.Get(r)
		if ctx.ModelOut != nil {
			outErr := getEncoder(w, r)(ctx.ModelOut)
			if outErr != nil {
				say.Error(outErr)
				w.WriteHeader(500)
				fmt.Fprintf(w, "An error occurred.")
			}
		}
	}

	return http.HandlerFunc(fn)
}

func getMimeType(r *http.Request) string {
	path := strings.Split(r.URL.Path, ".")

	switch {
	case len(path) > 1:
		return path[1]
	case r.Header.Get("Content-Type") != "":
		return r.Header.Get("Content-Type")
	}

	return "json"
}

func getEncoder(w http.ResponseWriter, r *http.Request) (encodeFunc util.Encode) {
	pathMime := strings.Split(r.URL.Path, ".")

	var outMime string
	switch {
	case len(pathMime) > 1:
		outMime = pathMime[1]
	case r.Header.Get("Accept") != "":
		outMime = r.Header.Get("Accept")
	case r.Header.Get("Content-Type") != "":
		outMime = r.Header.Get("Content-Type")
	default:
		outMime = "application/json"
	}

	switch {
	case strings.HasSuffix(outMime, "json"):
		encodeFunc = json.NewEncoder(w).Encode
	case strings.HasSuffix(outMime, "xml"):
		encodeFunc = xml.NewEncoder(w).Encode
	default:
		encodeFunc = json.NewEncoder(w).Encode
	}
	return
}
